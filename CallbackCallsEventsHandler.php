<?php

namespace AmiEvents;

use Yii;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\NewCalleridEvent;
use PAMI\Message\Event\NewAccountCodeEvent;
use PAMI\Message\Event\TransferEvent;
use PAMI\Message\Event\HangupEvent;
use models\Country;
use models\IncomingCall;

/**
 * Класс для обработки событий callback-звонков
 */
class CallbackCallsEventsHandler implements IEventListener
{
    /**
     * Признак callback-звонков
     */
    const AUTOCALLS = 'AUTOCALLS';

    /**
     * Значение поля "extra" для перевода звонка на другой добавочный
     */
    const EXTRA_TRANSFERRED = 'transferred';

    /**
     * @var IncomingCallsServiceInterface
     */
    protected $incomingCallsService;

    /**
     * IncomeCallsEventsHandler constructor.
     * @param IncomingCallsServiceInterface $incomingCallsService
     */
    public function __construct(IncomingCallsServiceInterface $incomingCallsService)
    {
        $this->incomingCallsService = $incomingCallsService;
    }

    /**
     * @inheritdoc
     */
    public function handle(EventMessage $event)
    {
        try {
            if ($event instanceof NewCalleridEvent) {
                $event->handleResult = $this->handleNewCalleridEvent($event);
            } elseif ($event instanceof NewAccountCodeEvent) {
                $event->handleResult = $this->handleNewAccountCodeEvent($event);
            } elseif ($event instanceof TransferEvent) {
                // Обработка случая перевода звонка на другой добавочный БЕЗ предварительного СОГЛАСОВАНИЯ
                $event->handleResult = $this->handleTransferEvent($event);
            } elseif ($event instanceof HangupEvent) {
                $event->handleResult = $this->handleHangupEvent($event);
            }
        } catch (\Exception $e) {
            Yii::warning(
                'Ошибка при обработке события: ' . $e->getMessage() . PHP_EOL . 'Event source:' . PHP_EOL
                . var_export($event, true) . PHP_EOL . 'Error trace: ' . PHP_EOL . $e->getTraceAsString() . PHP_EOL,
                __METHOD__
            );
        }
    }

    /**
     * Метод выполняет обработку newcallerid-события
     * @param NewCalleridEvent $event
     * @return mixed
     */
    protected function handleNewCalleridEvent(NewCalleridEvent $event)
    {
        if (preg_match('(^4\d{3}|21\d{2}$)', ($internalPhone = $event->getCallerIDNum()))) {
            // Только звонки c добавочными 21__ и 4___
            if ($event->getCallerIDName() === self::AUTOCALLS) {
                // Послупление исходящего звонка для callback сопровождается значением calleridname=AUTOCALLS
                return $this->startCall($event->getUniqueID(), [
                    'internal_phone' => $internalPhone,
                ]);
            } elseif (($initialIncomingCall = $this->incomingCallsService->findByCallId($event->getUniqueID()))) {
                // Обработка случая перевода звонка на другой добавочный С предварительным СОГЛАСОВАНИЕМ
                return $this->startCall($event->getUniqueID(), [
                    'internal_phone' => $internalPhone,
                    'telephone' => $initialIncomingCall->telephone,
                    'country_id' => $initialIncomingCall->country_id,
                    'cl_client_id' => $initialIncomingCall->cl_client_id ?: null,
                    'state' => IncomingCall::STATE_ACTIVE,
                    'extra' => self::EXTRA_TRANSFERRED,
                ]);
            }
        }
        return false;
    }

    /**
     * Метод выполняет обработку newaccountcode-события
     * @param NewAccountCodeEvent $event
     * @return mixed
     */
    protected function handleNewAccountCodeEvent(NewAccountCodeEvent $event)
    {
        if (
            ($phoneNumber = $this->getAccountCodePhoneNumberPart($event->getAccountCode()))
            && ($initialIncomingCall = $this->incomingCallsService->findByCallId($event->getUniqueID()))
            && $initialIncomingCall->type === IncomingCall::TYPE_CALLBACK
        ) {
            return $this->incomingCallsService->updateCall($event->getUniqueID(), [
                'country_id' => (
                    // Страну клиента определяем на основе префикса телефонного номера, если он указан
                    (strlen($phoneNumber) === 14 && substr($phoneNumber, 0, 2) === '11') ? Country::RUS : Country::FR
                ),
                'telephone' => $this->normalizePhoneNumber($phoneNumber),
                'state' => IncomingCall::STATE_ACTIVE,
            ]);
        }
        return false;
    }

    /**
     * Метод выполняет обработку transfer-события
     * @param TransferEvent $event
     * @return mixed
     */
    protected function handleTransferEvent(TransferEvent $event)
    {
        if (
            ($initialIncomingCall = $this->incomingCallsService->findByCallId($event->getUniqueID()))
            && $initialIncomingCall->type === IncomingCall::TYPE_CALLBACK
        ) {
            return $this->startCall($event->getTargetUniqueID(), [
                'internal_phone' => $event->getTransferExten(),
                'telephone' => $initialIncomingCall->telephone,
                'country_id' => $initialIncomingCall->country_id,
                'cl_client_id' => $initialIncomingCall->cl_client_id ?: null,
                'initial_call_id' => $initialIncomingCall->call_id,
                'state' => IncomingCall::STATE_ACTIVE,
                'extra' => self::EXTRA_TRANSFERRED,
            ]);
        }
        return false;
    }

    /**
     * Метод выполняет обработку hangup-события
     * @param HangupEvent $event
     * @return mixed
     */
    protected function handleHangupEvent(HangupEvent $event)
    {
        if (in_array(self::AUTOCALLS, [$event->getKey('connectedlinename'), $event->getKey('calleridname')])) {
            // Сначала пробуем найти и завершить звонок по идентификатору, иначе - завершаем звонки по номеру телефона
            if ($this->incomingCallsService->findByCallId($event->getUniqueID())) {
                return $this->incomingCallsService->finishByCallId($event->getUniqueID(), IncomingCall::TYPE_CALLBACK);
            } elseif (($telephone = $this->getAccountCodePhoneNumberPart($event->getKey('accountcode')))) {
                return $this->incomingCallsService->finishByTelephone($this->normalizePhoneNumber($telephone), IncomingCall::TYPE_CALLBACK);
            }
        }
        return false;
    }

    /**
     * @param $accountCode
     * @return bool|string
     */
    protected function getAccountCodePhoneNumberPart($accountCode)
    {
        if (
            strlen($accountCode)
            && count(($accountCodeArray = explode(',', $accountCode)))
            && strlen(($phoneNumber = end($accountCodeArray))) > 11
        ) {
            return (string)$phoneNumber;
        }

        return false;
    }

    /**
     * @param $phoneNumber
     * @return bool|string
     */
    protected function normalizePhoneNumber($phoneNumber)
    {
        return substr(trim($phoneNumber), strlen(trim($phoneNumber)) - 10);
    }

    /**
     * @param string $callId
     * @param array $data
     * @return IncomingCall
     */
    protected function startCall($callId, $data, $debug = false)
    {
        return $this->incomingCallsService->startCall($callId, array_merge($data, [
            'type' => IncomingCall::TYPE_CALLBACK,
        ]), $debug);
    }
}

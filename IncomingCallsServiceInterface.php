<?php

namespace AmiEvents;

use models\Client;
use models\IncomingCall;

/**
 * Сервис для работы с поступающими звонками
 * @package edit\modules\clientcard\components
 */
interface IncomingCallsServiceInterface
{
    /**
     * Метод выполняет создание и возвращает новый звонок на основе переданного массива данных
     * @param string $callId
     * @param array $data
     * @return IncomingCall
     */
    public function startCall($callId, array $data);

    /**
     * Метод выполняет обновление звонка
     * @param string $callId
     * @param array $data
     * @return IncomingCall|false
     */
    public function updateCall($callId, $data);

    /**
     * Метод возвращает изначально поступивший звонок (который затем мог быть переведён)
     * @param $telephone
     * @return IncomingCall|null
     */
    public function getInitialIncomeCall($telephone);

    /**
     * Метод выполняет завершение звонка по идентификатору звонка
     * @param string $callId
     * @param int $type
     * @return IncomingCall|false
     */
    public function finishByCallId($callId, $type = null);

    /**
     * Метод выполняет завершение звонков по номеру телефона
     * @param string $telephone
     * @param null|int $type
     * @return IncomingCall|false
     */
    public function finishByTelephone($telephone, $type = null);

    /**
     * Метод выполняет удаление звонков по указанному номеру телефона
     * @param $telephone
     * @return int Количество обработанных звонков
     */
    public function deleteCallsByTelephone($telephone);

    /**
     * Метод выполняет поиск и возвращает звонок по переданному идентификатору
     * @param $callId
     * @return IncomingCall
     */
    public function findByCallId($callId);

    /**
     * @param $incomingCallId
     * @return IncomingCall|null
     */
    public function find($incomingCallId);

    /**
     * Метод выполняет привязку поступившего звонка и клиента
     * @param IncomingCall $incomingCall
     * @param Client $client
     * @return bool
     */
    public function bindIncomingCallToClient(IncomingCall $incomingCall, Client $client);

    /**
     * Метод проверяет существование привязки поступившего звонка и карточки клиента
     * @param IncomingCall $incomingCall
     * @param Client $client
     * @return bool
     */
    public function isIncomingCallBindedToClient(IncomingCall $incomingCall, Client $client);

    /**
     * Метод выполняет пометку звонка как "Открыт"
     * @param IncomingCall $incomingCall
     * @return bool
     */
    public function setHasBeenOpened(IncomingCall $incomingCall);

    /**
     * Метод выполняет пометку звонка как "Обработан"
     * @param IncomingCall $incomingCall
     * @return bool
     */
    public function setHasBeenProcessed(IncomingCall $incomingCall);

    /**
     * Метод выполняет удаление истёкших звонков
     */
    public function deleteExpiredCalls();
}

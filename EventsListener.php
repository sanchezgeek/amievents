<?php

namespace AmiEvents;

use yii\base\Object;
use PAMI\Client\IClient;
use PAMI\Client\Impl\ClientImpl;
use React\EventLoop\Factory;
use yii\di\Instance;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\NewCalleridEvent;
use PAMI\Message\Event\NewAccountCodeEvent;
use PAMI\Message\Event\TransferEvent;
use PAMI\Message\Event\NewchannelEvent;
use PAMI\Message\Event\NewstateEvent;
use PAMI\Message\Event\HangupEvent;

/**
 * Класс для обработки событий от AMI
 */
class EventsListener extends Object
{
    /**
     * Параметры для подключения
     */
    public $host;
    public $scheme;
    public $port;
    public $username;
    public $secret;
    public $connectTimeout;
    public $readTimeout;

    /**
     * @var
     */
    protected $loop;

    /**
     * @var IClient|ClientImpl
     */
    protected $amiClient;

    /**
     * @var Интервал опроса
     */
    public $processEventsInterval = 0.5;

    /**
     * @var int Интервал запуска проверки состояния соединения
     */
    public $checkConnectionInterval = 10;

    /**
     * @var EventMessage
     */
    protected $lastReceivedMessage;

    /**
     * Запуск
     */
    public function run()
    {
        $this->amiClient = new ClientImpl([
            'host' => $this->host,
            'scheme' => $this->scheme,
            'port' => $this->port,
            'username' => $this->username,
            'secret' => $this->secret,
            'connect_timeout' => $this->connectTimeout,
            'read_timeout' => $this->readTimeout,
        ]);
        $this->loop = Factory::create();

        $this->amiClient->registerEventListener(Instance::ensure(IncomeCallsEventsHandler::class), function (EventMessage $event) {
            return !(bool)$event->handleResult && (
                $event instanceof NewchannelEvent
                || $event instanceof NewstateEvent
                || $event instanceof TransferEvent
                || $event instanceof HangupEvent
            );
        });
        $this->amiClient->registerEventListener(Instance::ensure(CallbackCallsEventsHandler::class), function (EventMessage $event) {
            return !(bool)$event->handleResult && (
                $event instanceof NewCalleridEvent
                || $event instanceof NewAccountCodeEvent
                || $event instanceof TransferEvent
                || $event instanceof HangupEvent
            );
        });
        $this->amiClient->registerEventListener([$this, 'logLastReceivedMessage']);
        $this->amiClient->open();
        $this->loop->addPeriodicTimer($this->processEventsInterval, [$this->amiClient, 'process']);
        $this->loop->addPeriodicTimer($this->checkConnectionInterval, [$this, 'checkConnectionState']);
        $this->loop->run();
    }

    /**
     * Логгирование последнего полученного сообщения
     * @param EventMessage $message
     */
    public function logLastReceivedMessage(EventMessage $message)
    {
        $this->lastReceivedMessage = [time() => $message];
    }

    /**
     * Метод проверяет необходимость и выполняет переподключение к серверу телефонии
     */
    public function checkConnectionState()
    {
        if ($this->lastReceivedMessage) {
            $lastMessageTimestamp = reset(array_keys($this->lastReceivedMessage));
            if (time() > $lastMessageTimestamp + $this->checkConnectionInterval) {
                $this->amiClient->close();
                $this->amiClient->open();
            }
        }
    }

    /**
     * Деструктор объекта
     */
    public function __destruct() {
        if ($this->loop) {
            $this->loop->stop();
        }

        if ($this->amiClient) {
            $this->amiClient->close();
        }
    }
}

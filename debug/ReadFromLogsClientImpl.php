<?php

namespace AmiEvents\debug;

use PAMI\Message\IncomingMessage;
use PAMI\Message\Message;

/**
 * Клиент, который выполняет чтение событий из логов, записанных с использованием CollectEventsHandler
 */
class ReadFromLogsClientImpl extends \PAMI\Client\Impl\ClientImpl
{
    /**
     * @inheritdoc
     */
    protected function getMessages()
    {
        $log = trim(
            trim(
                trim(
                    file_get_contents(CollectEventsHandler::getAllFilePath()),
                    "'"
                ),
                Message::EOL
            ),
            "'" . CollectFileTarget::DELIMETER
        );
        return $log ? explode("'" . CollectFileTarget::DELIMETER . PHP_EOL . "'", $log) : [];
    }

    /**
     * @inheritdoc
     * Обработка выполняется только один раз
     */
    public function process()
    {
        parent::process(); die;
    }

    /**
     * @inheritdoc
     * Обработка сообщений выполняется с задержкой 200мс
     */
    protected function dispatch(IncomingMessage $message)
    {
        parent::dispatch($message);
        usleep(100000);
    }
}

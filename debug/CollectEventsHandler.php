<?php

namespace AmiEvents\debug;

use Yii;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;

/**
 * Обработчик событий, который осуществляет логгирование событий.
 * Для каждого типа поступающих событий будет создан отдельный файл.
 */
class CollectEventsHandler implements IEventListener
{
    const LOG_CATEGORY_PREFIX = 'pamiCollect';

    /**
     * @var array Названия категорий событий, которые не попадут в логи
     */
    protected $exceptEvents = [
        'dtmf',
        'registry',
        'softhanguprequest',
        'musiconhold',
        'peerstatus',
        'varset',
        'queuememberstatus',
        'queuememberpaused',
        'rtcpsent',
        'rtcpreceived',
        'fullybooted',
        'rtcpreceived',
        'newexten',
        'hanguprequest'
    ];

    public function __construct()
    {
        $exceptLogCategories = [];
        foreach ($this->exceptEvents as $exceptEvent) {
            $exceptLogCategories[] = $this->getEventLogCategory($exceptEvent);
        }
        $this->addLogTarget($this->getEventLogCategory('*'), self::getAllFilePath(), $exceptLogCategories); // Для логгирования всех событий
    }

    /**
     * @inheritdoc
     */
    public function handle(EventMessage $event)
    {
        $eventName = strtolower($event->getKey('event'));
        $eventLogCategory = $this->getEventLogCategory($eventName);
        $this->addLogTarget($eventLogCategory, self::getCategoriesDirectory() . "/{$eventName}.log"); // Для логгирования каждой категории событий в отдельности
        \Yii::info(var_export($event->getRawContent(), true), $eventLogCategory);
    }

    /**
     * @param string $eventName
     * @return string
     */
    protected function getEventLogCategory($eventName)
    {
        return self::LOG_CATEGORY_PREFIX . '/' . $eventName;
    }

    /**
     * @param $eventLogCategory
     * @param $logFilePath
     * @param array $except
     */
    protected function addLogTarget($eventLogCategory, $logFilePath, $except = [])
    {
        $categories = [];
        foreach (\Yii::$app->log->targets as $target) {
            $categories = array_merge($categories, $target->categories);
        }
        if (!in_array($eventLogCategory, $categories)) {
            \Yii::$app->log->targets[] = \Yii::createObject([
                'class' => CollectFileTarget::class,
                'levels' => ['info'],
                'except' => $except,
                'logVars' => [],
                'categories' => [$eventLogCategory],
                'logFile' => $logFilePath,
                'exportInterval' => 1,
            ]);
        }
    }

    /**
     * @var string Имя записываемого варинта (произвольное значение)
     */
    protected static $caseName = 'callback_redirect_one';

    /**
     * @return bool|string Корневая директория для записи логов
     */
    static public function getRootDirectory()
    {
        return Yii::getAlias('@runtime/logs/pami/collect/' . self::$caseName);
    }

    /**
     * @return string Путь к общему логу
     */
    static public function getAllFilePath()
    {
        return self::getRootDirectory() . '/all.log';
    }

    /**
     * @return string Директория для записи логов по каждой категории событий в отдельности
     */
    static public function getCategoriesDirectory()
    {
        return self::getRootDirectory() . '/categories';
    }
}

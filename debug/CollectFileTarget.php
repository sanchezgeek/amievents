<?php

namespace AmiEvents\debug;

use yii\helpers\VarDumper;

/**
 * Специальный target для записи логов
 */
class CollectFileTarget extends \yii\log\FileTarget
{
    /**
     * Разделитель сообщений
     */
    const DELIMETER = "------------";

    /**
     * @inheritdoc
     */
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;
        if (!is_string($text)) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string) $text;
            } else {
                $text = VarDumper::export($text);
            }
        }
        return $text . self::DELIMETER;
    }
}

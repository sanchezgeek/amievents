<?php

namespace AmiEvents\debug;

use Yii;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\Factory\Impl\EventFactoryImpl;
use PAMI\Message\Message;

/**
 * Класс для фильтрации логов
 */
class LogsFilter
{
    /**
     * Режим поиска (строгое/нестрогое соответствие)
     */
    const NOT_STRICT = false;
    const STRICT = true;

    const LOG_CATEGORY = 'pamiCollectFiltered';

    /**
     * @var array Массив ключей для определения сввязанных событий
     */
    protected $relatedKeys = [
        'uniqueid' => self::NOT_STRICT,
    ];

    /**
     * @var array Массив значений для фильтрации
     * [
     *     value => strictMode,
     *     ...
     * ]
     */
    protected $initialFilterValues = [];

    /**
     * @var Исключаемые файлы
     */
    protected $exceptFiles = [];

    /**
     * Пример настройки фильтров (ищем события, связанные со звонкм по номеру 9035933334 и его переводом на добавочный 2112)
     *
    protected $initialFilterValues = [
        9035933334 => self::NOT_STRICT,
        2112 => self::STRICT,
    ];
    protected $exceptFiles = [
        'varset',
        'queuememberstatus',
        'newexten',
        'musiconhold',
        'leave',
    ];
     */

    /**
     * @var EventFactoryImpl
     */
    protected $eventFactory;

    /**
     * LogsFilter constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->eventFactory = new EventFactoryImpl();
        if (empty($this->initialFilterValues)) {
            throw new \Exception('Необходимо указать значения для фильтрации');
        }
        $fileNameHash = implode(',', array_keys($this->initialFilterValues));
        if (!empty($this->exceptFiles)) {
            $fileNameHash .= '_except_' . implode(',', $this->exceptFiles);
        }
        Yii::$app->log->targets[] = Yii::createObject([
            'class' => LogsFilterFileTarget::class,
            'levels' => ['info'],
            'logVars' => [],
            'categories' => [self::LOG_CATEGORY],
            'logFile' => CollectEventsHandler::getRootDirectory() . "/filtered_by/{$fileNameHash}.log",
            'exportInterval' => 1,
        ]);
    }

    /**
     * Запуск фильтра
     */
    public function run()
    {
        $files = FileHelper::findFiles(CollectEventsHandler::getCategoriesDirectory());
        $exceptFiles = $this->exceptFiles;
        $files = array_filter($files, function($file) use ($exceptFiles) {
            return !in_array(pathinfo($file)['filename'], $exceptFiles); 
        });
        $messages = [];
        foreach ($files as $file) {
            $messages = array_merge($messages, $this->parseMessages($file));
        }
        
        $eventFactory = $this->eventFactory;
        $events = array_map(function($message) use ($eventFactory) {
            return $eventFactory->createFromRaw($message);
        }, $messages);
        usort($events, function(EventMessage $one, EventMessage $two) {
            return $one->getKey('timestamp') > $two->getKey('timestamp');
        });
        $filterValues = $this->getFilterValues($events, $this->initialFilterValues);
        $filteredEvents = array_filter($events, function (EventMessage $event) use ($filterValues) {
            foreach ($filterValues as $filterValue => $searchMode) {
                $pattern = $searchMode === self::STRICT ? "/\b$filterValue\b/" : "/$filterValue/";
                if (preg_match($pattern, $event->getRawContent())) {
                    return true;
                }
            }
            return false;
        });
        foreach ($filteredEvents as $filteredEvent) {
            /** @var EventMessage $filteredEvent */
            \Yii::info(date('Y-m-d H:i:s', $filteredEvent->getKey('timestamp')) . PHP_EOL . var_export($filteredEvent->getRawContent(), true), self::LOG_CATEGORY);
        }
    }

    /**
     * @param $filePath
     * @return array
     */
    protected function parseMessages($filePath)
    {
        $log = trim(
            trim(
                trim(file_get_contents($filePath), "'"),
                Message::EOL
            ),
            "'" . CollectFileTarget::DELIMETER
        );
        return $log ? explode("'" . CollectFileTarget::DELIMETER . PHP_EOL . "'", $log) : [];
    }

    /**
     * Возвращает итоговый массив значений для фильтрации с учетом связанных событий
     * @param $events
     * @param $initialFilterValues
     * @return array
     */
    protected function getFilterValues($events, $initialFilterValues)
    {
        $relatedFilterValues = [];
        $relatedKeys = $this->relatedKeys;
        array_map(function (EventMessage $event) use ($initialFilterValues, &$relatedFilterValues, $relatedKeys) {
            foreach ($initialFilterValues as $filterValue => $searchMode) {
                $pattern = $searchMode === self::STRICT ? "/\b$filterValue\b/" : "/$filterValue/";
                if (preg_match($pattern, $event->getRawContent())) {
                    foreach ($event->getKeys() as $key => $value) {
                        if (isset($relatedKeys[$key]) && !is_null($value)) {
                            $relatedFilterValues[$value] = ($relatedKeySearchMode = $relatedKeys[$key]);
                        }
                    }
                }
            }
        }, $events);
        return !empty(($newFilterValues = array_diff_key($relatedFilterValues, $initialFilterValues)))
            ? $this->getFilterValues($events, $initialFilterValues + $newFilterValues)
            : $initialFilterValues
        ;
    }
}

/**
 * Специальный target для записи отфильтрованного лога
 */
class LogsFilterFileTarget extends \yii\log\FileTarget
{
    /**
     * @inheritdoc
     */
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;
        if (!is_string($text)) {
            // exceptions may not be serializable if in the call stack somewhere is a Closure
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string) $text;
            } else {
                $text = VarDumper::export($text);
            }
        }
        return $text . PHP_EOL . PHP_EOL . PHP_EOL;
    }
}

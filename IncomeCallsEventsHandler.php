<?php

namespace AmiEvents;

use Yii;
use PAMI\Listener\IEventListener;
use PAMI\Message\Event\EventMessage;
use PAMI\Message\Event\NewchannelEvent;
use PAMI\Message\Event\NewstateEvent;
use PAMI\Message\Event\TransferEvent;
use PAMI\Message\Event\HangupEvent;
use models\Country;
use models\IncomingCall;

/**
 * Класс для обработки событий входящих звонков
 */
class IncomeCallsEventsHandler implements IEventListener
{
    /**
     * Значение поля "extra" для предварительного звонка при переводе на другой добавочный
     */
    const EXTRA_PRELIMINARY_CALL = 'preliminaryCall';

    /**
     * Значение поля "extra" для перевода звонка на другой добавочный
     */
    const EXTRA_TRANSFERRED = 'transferred';

    /**
     * Up-состояние - между конечной точкой и сервером телефонии устанавлен канал связи
     */
    const CHANNEL_STATE_UP = 6;

    /**
     * Типы transfer-события
     */
    const TRANSFER_TYPE_BLIND = 'Blind';
    const TRANSFER_TYPE_ATTENDED = 'Attended';

    /**
     * @var IncomingCallsServiceInterface
     */
    protected $incomingCallsService;

    /**
     * @var array Линии, на которые могут быть перенаправлены основные линии
     */
    protected $redirectedLines = [
        '88001234567' => '74991234567',
        '88002345678' => '74992345678',
        '88003456789' => '74993456789',
        '88004567890' => '74994567890',
    ];

    /**
     * @var array Входящие линии
     */
    protected $rusLines = [
        '74991234567',
        '88001234567',
        '74956496491',
        '74956496492',
    ];

    /**
     * @var array Входящие линии
     */
    protected $frLines = [
        '74958001534',
        '78005001532',
        '88005001532',
        '78005001534',
        '88005001534',
        '78005001535',
        '88005001535',
    ];

    /**
     * IncomeCallsEventsHandler constructor.
     * @param IncomingCallsServiceInterface $incomingCallsService
     */
    public function __construct(IncomingCallsServiceInterface $incomingCallsService)
    {
        $this->incomingCallsService = $incomingCallsService;
    }

    /**
     * @inheritdoc
     */
    public function handle(EventMessage $event)
    {
        try {
            if ($event instanceof NewchannelEvent) {
                $event->handleResult = $this->handleNewChannelEvent($event);
            } elseif ($event instanceof NewstateEvent) {
                $event->handleResult = $this->handleNewStateEvent($event);
            } elseif ($event instanceof TransferEvent) {
                // Обработка случая перевода звонка на другой добавочный С предварительным СОГЛАСОВАНИЕМ
                $event->handleResult = $this->handleTransferEvent($event);
            } elseif ($event instanceof HangupEvent) {
                $event->handleResult = $this->handleHangupEvent($event);
            }
        } catch (\Exception $e) {
            Yii::warning(
                'Ошибка при обработке события: ' . $e->getMessage() . PHP_EOL . 'Event source:' . PHP_EOL
                . var_export($event, true) . PHP_EOL . 'Error trace: ' . PHP_EOL . $e->getTraceAsString() . PHP_EOL,
                __METHOD__
            );
        }
    }

    /**
     * Метод выполняет обработку newchannel-события
     * @param NewchannelEvent $event
     * @return mixed
     */
    protected function handleNewChannelEvent(NewchannelEvent $event)
    {
        $line = $this->normalizeLine($event->getExtension());
        if (in_array($line, array_merge($this->rusLines, $this->frLines))) {
            // Обработка входящего звонка
            return $this->startCall($event->getUniqueID(), [
                'line' => $line,
                'country_id' => $this->getCountryId($line),
                'telephone' => $this->normalizePhoneNumber($event->getCallerIDNum()),
            ]);
        } elseif (
            preg_match('(^4\d{3}|21\d{2}$)', $line)
            && (
                $initialIncomingCall = IncomingCall::find()
                    ->where(['internal_phone' => $event->getCallerIDNum()])
                    ->andWhere(['type' => IncomingCall::TYPE_INCOME])
                    ->one()
            )
        ) {
            // Обработка предварительного звонка при переводе на другой добавочный
            return $this->startCall($event->getUniqueID(), [
                'line' => $initialIncomingCall->line,
                'country_id' => $initialIncomingCall->country_id,
                'telephone' => $initialIncomingCall->telephone,
                'internal_phone' => $line,
                'initial_call_id' => $initialIncomingCall->getInitialCallId(),
                'cl_client_id' => $initialIncomingCall->cl_client_id,
                'extra' => self::EXTRA_PRELIMINARY_CALL
            ]);
        }
        return false;
    }

    /**
     * Метод выполняет обработку newstate-события
     * @param NewstateEvent $event
     * @return mixed
     */
    protected function handleNewStateEvent(NewstateEvent $event)
    {
        if ($event->getChannelState() == self::CHANNEL_STATE_UP) {
            if (
                strlen(($telephone = $this->normalizePhoneNumber($event->getConnectedLineNum()))) > 4
                // $initialIncomingCall необходим для получения линии, на которую поступил звонок
                && ($initialIncomingCall = $this->incomingCallsService->getInitialIncomeCall($telephone))
            ) {
                if (preg_match('(^4\d{3}|21\d{2}$)', ($internalPhone = $event->getCallerIDNum()))) {
                    // Обрабатываем звонок, если он переключён на добавочные 21__ или 4___
                    if (!(
                        $existedIncomingCall = IncomingCall::find()
                            ->where(['initial_call_id' => $initialIncomingCall->call_id])
                            ->andWhere(['internal_phone' => $internalPhone])
                            ->one()
                    )) {
                        // Создание звонка делаем только в случае, если ещё нет записи с таким добавочным
                        return $this->startCall($event->getUniqueID(), [
                            'line' => $initialIncomingCall->line,
                            'country_id' => $initialIncomingCall->country_id,
                            'telephone' => $initialIncomingCall->telephone,
                            'cl_client_id' => $initialIncomingCall->cl_client_id,
                            'initial_call_id' => $initialIncomingCall->getInitialCallId(),
                            'internal_phone' => $internalPhone,
                            'state' => IncomingCall::STATE_ACTIVE,
                        ]);
                    }
                } else {
                    // Иначе (если звонок переключён на другой добавочный) - удаляем информацию о звонках на этот номер
                    return $this->incomingCallsService->deleteCallsByTelephone($telephone);
                }
            }
        }
        return false;
    }

    /**
     * Метод выполняет обработку transfer-события
     * @param TransferEvent $event
     * @return mixed
     */
    protected function handleTransferEvent(TransferEvent $event)
    {
        switch ($event->getTransferType()) {
            case self::TRANSFER_TYPE_ATTENDED:
                $fromUniqueId = $event->getUniqueID();
                $toUniqueId = $event->getTargetUniqueID();
                if (
                    ($initialIncomingCall = $this->incomingCallsService->findByCallId($fromUniqueId))
                    && $initialIncomingCall->type === IncomingCall::TYPE_INCOME
                    && ($preliminaryCall = $this->incomingCallsService->findByCallId($toUniqueId))
                    && $preliminaryCall->extra === self::EXTRA_PRELIMINARY_CALL
                ) {
                    // При переводе на предварительный звонок, обновляем его состояние на "Активный"
                    return $this->incomingCallsService->updateCall($toUniqueId, [
                        'state' => IncomingCall::STATE_ACTIVE,
                        'extra' => self::EXTRA_TRANSFERRED,
                    ]);
                }
                break;
            case self::TRANSFER_TYPE_BLIND:
                if ($initialIncomingCall = $this->incomingCallsService->findByCallId($event->getTargetUniqueID())) {
                    return $this->startCall($event->getTargetUniqueID(), [
                        'line' => $initialIncomingCall->line,
                        'country_id' => $initialIncomingCall->country_id,
                        'telephone' => $initialIncomingCall->telephone,
                        'internal_phone' => $event->getTransferExten(),
                        'initial_call_id' => $initialIncomingCall->call_id,
                        'state' => IncomingCall::STATE_ACTIVE,
                        'extra' => self::EXTRA_TRANSFERRED,
                    ]);
                }
                break;
            default:

        }
        return false;
    }

    /**
     * Метод выполняет обработку hangup-события
     * @param HangupEvent $event
     * @return mixed
     */
    protected function handleHangupEvent(HangupEvent $event)
    {
        return $this->incomingCallsService->finishByCallId($event->getUniqueID(), IncomingCall::TYPE_INCOME);
    }

    /**
     * @param string $line
     * @return string
     */
    protected function normalizeLine($line)
    {
        return in_array($line, $this->redirectedLines) ? array_search($line, $this->redirectedLines) : $line;
    }

    /**
     * @param $phoneNumber
     * @return bool|string
     */
    protected function normalizePhoneNumber($phoneNumber)
    {
        return substr(trim($phoneNumber), strlen(trim($phoneNumber)) - 10);
    }

    /**
     * Метод возвращает идентификатор страны, в контексте которой был принят звонок по указанной линии
     * @param string $line
     * @return int
     */
    protected function getCountryId($line)
    {
        return in_array($line, $this->rusLines) ? Country::RUS : Country::FR;
    }

    /**
     * @param string $callId
     * @param array $data
     * @return IncomingCall
     */
    protected function startCall($callId, $data)
    {
        return $this->incomingCallsService->startCall($callId, array_merge($data, [
            'type' => IncomingCall::TYPE_INCOME,
        ]));
    }
}
